<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Patient Information System</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
   <link rel="stylesheet" href="vendors/iconfonts/font-awesome/css/font-awesome.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/logo_thumbnail.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="dash.php">
          <img src="images/logo.png" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="dash.php">
          <img src="images/logo_thumbnail.png" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            <a href="details.php" class="nav-link">
             <i class="mdi mdi-account-details"></i>Patient Details</a>
          </li>
          <li class="nav-item active">
            <a href="history.php" class="nav-link">
              <i class="mdi mdi-clock-alert"></i>Medical History</a>
          </li>
          <li class="nav-item active">
            <a href="reports.php" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>Daily Report</a>
          </li>
          <li class="nav-item active">
            <a href="immunisations.php" class="nav-link">
              <i class="mdi mdi-needle"></i>Immunisations</a>
          </li>
          <li class="nav-item">
            <a href="medications.php" class="nav-link">
              <i class="mdi mdi-pill"></i>Medications</a>
          </li>
          <li class="nav-item active">
            <a href="allergies.php" class="nav-link">
              <i class="mdi mdi-flower"></i>Allergies</a>
          </li>
          <li class="nav-item active">
            <a href="results.php" class="nav-link">
              <i class="mdi mdi-heart-pulse"></i>Test Results</a>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Dr. Jenkins!</span>
              <img class="img-xs rounded-circle" src="images/faces/face17.jpg" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                  </div>
                </div>
              </a>
              <a class="dropdown-item">
                Change Password
              </a>
              <a class="dropdown-item">
                Sign Out
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="images/faces/face17.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Dr. Jenkins</p>
                  <div>
                    <small class="designation text-muted">Consultant</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dash.php">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="newpatient.php">
              <i class="menu-icon mdi mdi-account-plus"></i>
              <span class="menu-title">Add New Patient</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="details.php">
              <i class="menu-icon mdi mdi-account-box"></i>
              <span class="menu-title">Patient Search</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="menu-icon mdi mdi-alien"></i>
              <span class="menu-title">Extra Features Soon!</span>
            </a>
          </li>
          <li class="nav-item">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="text-wrapper">
                 <button class="btn btn-danger widthbtn" onclick="window.location.href='index.php'">Log Out
                <i class="mdi mdi-run"></i>
              </button>
              </div>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
          <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-right">
                      <i class="mdi mdi-clock-alert text-info icon-lg"></i>
                    </div>
                    <div class="float-left">
                      <h1 class="display-4">Medical History</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright � 2018 404 Coursework Not Found. All rights reserved. Hand-crafted in
              <i class="mdi mdi-wordpress text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
<?php
?>

<?php
?>