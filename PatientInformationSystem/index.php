<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Login/PatientRequest</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="css/login.css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>

<body>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">
				<div class="panel-heading">

					<div class="row">


						<h3>Staff Login</h3>

					</div>
					<hr>
				</div>



				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<form id="login-form" action="LoginLogic.php" method="post"
								role="form" style="display: block;">
								<div class="form-group">
									<input type="text" name="staff_id" id="staff_id" tabindex="1"
										class="form-control" placeholder="Staff ID" value="">
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password"
										tabindex="2" class="form-control" placeholder="Password">

								</div>
																			<?php

                $Color = "red";

                if (isset($_SESSION['message'])) {

                    echo '<div style="Color:' . $Color . '">' . $_SESSION['message'] . '</div>';

                    unset($_SESSION['message']);
                }

                ?>
									<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit"
												tabindex="3" class="form-control btn btn-login"
												value="Log In">
										</div>
									</div>
								</div>
							</form>



						</div>
					</div>
				</div>

			</div>
		</div>



	</div>


		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">

						<div class="row">


							<h3>Patient Data Request Form</h3>

						</div>
						<hr>



						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<form id="patient_request_form" action="LoginLogic.php" method="post" role="form"
										style="display: block;">
										<div class="form-group">
											<input type="text" name="patient_id" id="patient_id"
												tabindex="4" class="form-control" placeholder="Patient ID"
												value="">
										</div>
										<div class="form-group">
											<input type="text" name="date_of_birth" id="date_of_birth"
												tabindex="5" class="form-control" placeholder="yyyy/mm/dd">
										</div>
										<div class="form-group">
											<input type="text" name="patient_postcode"
												id="patient_postcode" tabindex="6" class="form-control"
												placeholder="PostCode">
										</div>
										
										
											<?php

                $Color = "red";

                if (isset($_SESSION['patient_message'])) {

                    echo '<div style="Color:' . $Color . '">' . $_SESSION['patient_message'] . '</div>';

                    unset($_SESSION['patient_message']);
                }

                ?>
										
										
										
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6 col-sm-offset-3">
													<input type="submit" name="patient-submit"
														id="patient-submit" tabindex="7"
														class="form-control btn btn-login" value="Submit">
												</div>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		
















</body>
</html>