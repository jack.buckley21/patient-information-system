<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Patient Information System</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="vendors/iconfonts/font-awesome/css/font-awesome.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="images/logo_thumbnail.png" />

  <?php
include_once 'config.php';
session_start();
?>

</head>

<body>

<?php
$sql = "SELECT * from patient_personal_details;";
$result = mysqli_query($mysqli, $sql);
$row_count = $result->num_rows;

$staff_surname = "";
$sql2 = "SELECT staff_last_name from staff_details where staff_id = " . $_SESSION['staffid'];
$resultSurname = mysqli_query($mysqli, $sql2);
$rows = mysqli_fetch_array($resultSurname);

foreach ($rows as $found) {
    $staff_surname = $found;
}

$staff_gender = "";
$staff_title = "";
$sql3 = "SELECT staff_gender from staff_details where staff_id = " . $_SESSION['staffid'];
$resultGender = mysqli_query($mysqli, $sql3);
$rows = mysqli_fetch_array($resultGender);

foreach ($rows as $found) {
    $staff_gender = $found;
}

if ($_SESSION['role'] == "Doctor") {
    $staff_title = "Dr. ";
} else if ($_SESSION['role'] == "Consultant") {
    $staff_title = "Dr.";
} else if ($_SESSION['role'] == "Lab Technician") {
    $staff_title = "Dr. ";
} else if ($_SESSION['role'] == "Nurse" && $staff_gender == "M") {
    $staff_title = "Mr. ";
} else if ($_SESSION['role'] == "Nurse" && $staff_gender == "F") {
    $staff_title = "Miss. ";
} else if ($_SESSION['role'] == "Receptionist" && $staff_gender == "M") {
    $staff_title = "Mr. ";
} else if ($_SESSION['role'] == "Receptionist" && $staff_gender == "F") {
    $staff_title = "Miss. ";
} else {
    echo "Error.";
}
?>

  <div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<nav
			class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
			<div
				class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
				<a class="navbar-brand brand-logo" href="dash.php"> <img
					src="images/logo.png" alt="logo" />
				</a> <a class="navbar-brand brand-logo-mini" href="dash.php"> <img
					src="images/logo_thumbnail.png" alt="logo" />
				</a>
			</div>
			<div class="navbar-menu-wrapper d-flex align-items-center">

				<ul class="navbar-nav navbar-nav-right">
					<li class="nav-item dropdown d-none d-xl-inline-block"><a
						class="nav-link dropdown-toggle" id="UserDropdown" href="#"
						data-toggle="dropdown" aria-expanded="false">
              <?php

            echo '<span class="profile-text">Hello, ' . $staff_title . $staff_surname . '</span>'?>
              <img class="img-xs rounded-circle"
							src="images/faces/face17.jpg" alt="Profile image">
					</a>
						<div class="dropdown-menu dropdown-menu-right navbar-dropdown"
							aria-labelledby="UserDropdown">
							<a class="dropdown-item p-0">
								<div class="d-flex border-bottom">
									<div
										class="py-3 px-4 d-flex align-items-center justify-content-center">
									</div>
								</div>
							</a> <a class="dropdown-item"> Change Password </a> <a
								class="dropdown-item"> Sign Out </a>
						</div></li>
				</ul>
				<button
					class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
					type="button" data-toggle="offcanvas">
					<span class="mdi mdi-menu"></span>
				</button>
			</div>
		</nav>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<!-- partial:partials/_sidebar.html -->
			<nav class="sidebar sidebar-offcanvas" id="sidebar">
				<ul class="nav">
					<li class="nav-item nav-profile">
						<div class="nav-link">
							<div class="user-wrapper">
								<div class="profile-image">
									<img src="images/faces/face17.jpg" alt="profile image">
								</div>
								<div class="text-wrapper">
                  <?php

                echo '<p class="profile-name">' . $staff_title . $staff_surname . '</p>'?>
                  <div>
										<?php

        echo '<small class="designation text-muted">' . $_SESSION['role'] . '</small>'?> <span
											class="status-indicator online"></span>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="nav-item"><a class="nav-link" href="dash.php"> <i
							class="menu-icon mdi mdi-television"></i> <span
							class="menu-title">Dashboard</span>
					</a></li>
          <?php
        if ($_SESSION["role"] == "Receptionist") {
            echo '<li class="nav-item">
            <a class="nav-link" href="newpatient.php">
              <i class="menu-icon mdi mdi-account-plus"></i>
              <span class="menu-title">Add New Patient</span>
            </a>
          </li>';
        }
        ?>
					<li class="nav-item"><a class="nav-link" href="details.php"> <i
							class="menu-icon mdi mdi-account-box"></i> <span
							class="menu-title">Patient Search</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="allmedications.php"> <i
							class="menu-icon mdi mdi-pill"></i> <span
							class="menu-title">View All Medications</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="allimmunisations.php"> <i
							class="menu-icon mdi mdi-needle"></i> <span
							class="menu-title">View all Immunisations</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="#"> <i
							class="menu-icon mdi mdi-alien"></i> <span class="menu-title">Extra
								Features Soon!</span>
					</a></li>
					<li class="nav-item">
						<div class="nav-link">
							<div class="user-wrapper">
								<div class="text-wrapper">
									<button class="btn btn-danger widthbtn"
										onclick="window.location.href='index.php'">
										Log Out <i class="mdi mdi-run"></i>
									</button>
								</div>
							</div>

					</li>
				</ul>
			</nav>
			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div
							class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
							<div class="card card-statistics">
								<div class="card-body">
									<div class="clearfix">
										<div class="float-left">
											<i class="mdi mdi-account-location text-info icon-lg"></i>
										</div>
										<div class="float-right">
											<p class="mb-0 text-right">Patients</p>
											<div class="fluid-container">
                        <?php

                        echo "<h3 class='font-weight-medium text-right mb-0'>" . $row_count . "</h3>"?>
                      </div>
										</div>
									</div>
									<p class="text-muted mt-3 mb-0">
										<i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>
										12% up on last month
									</p>
								</div>
							</div>
						</div>
						<div
							class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
							<div class="card card-statistics">
								<div class="card-body">
									<div class="clearfix">
										<div class="float-left">
											<i class="mdi mdi-hospital text-danger icon-lg"></i>
										</div>
										<div class="float-right">
											<p class="mb-0 text-right">Incidents</p>
											<div class="fluid-container">
												<h3 class="font-weight-medium text-right mb-0">3</h3>
											</div>
										</div>
									</div>
									<p class="text-muted mt-3 mb-0">
										<i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Down
										35% on last month
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row"></div>
					<div class="row"></div>
					<div class="row"></div>
					<div class="row"></div>
				</div>
				<!-- content-wrapper ends -->
				<!-- partial:partials/_footer.html -->
				<footer class="footer">
					<div class="container-fluid clearfix">
						<span
							class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright
							� 2018 404 Coursework Not Found. All rights reserved.
							Hand-crafted in <i class="mdi mdi-wordpress text-danger"></i>
						</span>
					</div>
				</footer>
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<!-- plugins:js -->
	<script src="vendors/js/vendor.bundle.base.js"></script>
	<script src="vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<script src="js/off-canvas.js"></script>
	<script src="js/misc.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script src="js/dashboard.js"></script>
	<!-- End custom js for this page-->
</body>

</html>
