 <?php
include 'index.php';
include 'config.php';
?>

 <?php

if (isset($_POST['login-submit'])) {
    $staff_id = $_POST['staff_id'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM login WHERE staff_id = '$staff_id' AND password = '$password'";
    $result = mysqli_query($mysqli, $sql);
    $rows = mysqli_fetch_array($result);
    if ($rows['staff_id'] == $staff_id && $rows['password'] == $password) {

        $_SESSION['staffid'] = $staff_id;

        $result = mysqli_query($mysqli, "SELECT staff_role FROM staff_details WHERE staff_id = $staff_id");

        $rows = mysqli_fetch_array($result);

        foreach ($rows as $found) {
            $_SESSION['role'] = $found;
        }

        mysqli_close($mysqli);
        $loggedInStatus = 'TRUE';
        header("Location: dash.php");
    } else {
        mysqli_close($mysqli);
        $_SESSION['message'] = "Login failed, please try again.";
        header("Location: index.php");
    }
}

if (isset($_POST['patient-submit'])) {

    $patient_id = $_POST['patient_id'];
    $patient_date_of_birth = ($_POST['date_of_birth']);
    $patient_postcode = $_POST['patient_postcode'];

    $sql = "SELECT date_of_birth, postcode FROM patient_personal_details WHERE patient_id = $patient_id";

    $result = mysqli_query($mysqli, $sql);
    $row = mysqli_fetch_assoc($result);

    $db_date_of_birth = date('Y/m/d', strtotime($row['date_of_birth']));

    echo $db_date_of_birth . " " . $patient_date_of_birth . "\n";
    echo $row['postcode'] . " " . $patient_postcode;

    if ($db_date_of_birth == $patient_date_of_birth && $row['postcode'] == $patient_postcode) {

        $sql = "INSERT INTO patient_request (patient_id, date_requested)
                VALUES ($patient_id, CURDATE());";

        if (mysqli_query($mysqli, $sql)) {
            $_SESSION['patient_message'] = "New record created successfully";
            mysqli_close($mysqli);
            header("Location: index.php");
        } else {
            $_SESSION['patient_message'] = "Error: " . $sql . "<br>" . mysqli_error($mysqli);
        }
    } else {
        $_SESSION['patient_message'] = "Could not find Patient Details!";
        mysqli_close($mysqli);
        header("Location: index.php");
    }
}

?>