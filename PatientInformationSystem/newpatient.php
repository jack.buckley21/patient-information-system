<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Patient Information System</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
   <link rel="stylesheet" href="vendors/iconfonts/font-awesome/css/font-awesome.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/logo_thumbnail.png" />

<?php
include_once 'config.php';
?>

</head>

<body>
<?php

if (isset($_POST['submit'])) {
    $sql = "INSERT INTO patient_personal_details (first_name, last_name, date_of_birth, gender, address, postcode,
            mobile_number, email, nok_first_name, nok_last_name, nok_mobile, nok_address, nok_postcode)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

    $stmt = $mysqli->prepare($sql);

    $genderCap = strtoupper($_POST["gender"]);

    $stmt->bind_param("sssssssssssss", $_POST["first_name"], $_POST["last_name"], $_POST["date_of_birth"], $genderCap, $_POST["address"], $_POST["postcode"], $_POST["mobile_number"], $_POST["email"], $_POST["nok_first_name"], $_POST["nok_last_name"], $_POST["nok_mobile"], $_POST["nok_address"], $_POST["nok_postcode"]);

    if ($stmt->execute() == "TRUE") {
        $_SESSION['message'] = "Patient add successful.";
    } else {
        $_SESSION['message'] = "Patient add failure.";
    }
    $stmt->close();
}

?>

  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="dash.php">
          <img src="images/logo.png" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="dash.php">
          <img src="images/logo_thumbnail.png" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">

        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Dr. Jenkins!</span>
              <img class="img-xs rounded-circle" src="images/faces/face17.jpg" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                  </div>
                </div>
              </a>
              <a class="dropdown-item">
                Change Password
              </a>
              <a class="dropdown-item">
                Sign Out
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="images/faces/face17.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Dr. Jenkins</p>
                  <div>
                    <small class="designation text-muted">Consultant</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dash.php">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="newpatient.php">
              <i class="menu-icon mdi mdi-account-plus"></i>
              <span class="menu-title">Add New Patient</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="details.php">
              <i class="menu-icon mdi mdi-account-box"></i>
              <span class="menu-title">Patient Search</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="menu-icon mdi mdi-alien"></i>
              <span class="menu-title">Extra Features Soon!</span>
            </a>
          </li>
          <li class="nav-item">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="text-wrapper">
                 <button class="btn btn-danger widthbtn" onclick="window.location.href='index.php'">Log Out
                <i class="mdi mdi-run"></i>
              </button>
              </div>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
          	<div class="col-md-6 grid-margin stretch-card">
          		<div class="card">
            		<div class="card-body">
            			<h4 class="card-title">New Patient Form</h4>
            			<p class="card-description">This form is for adding a new patient to the system. Ensure that every
            			field is completed correctly.</p>

            			<?php

            $Color = "green";

            if (isset($_SESSION['message'])) {
                echo '<div style="Color:' . $Color . '">' . $_SESSION['message'] . '</br> </div>';
                unset($_SESSION['message']);
            }

            ?>

            			<form class="forms-sample" method="post">
            				<div class="form-group">
            					<label for="FirstName">First Name</label>
            					<input type="text" class="form-control" id="FirstName" name="first_name" placeholder="First Name" required>
            				</div>
            				<div class="form-group">
            					<label for="LastName">Last Name</label>
            					<input type="text" class="form-control" id="LastName"  name="last_name" placeholder="Last Name" required>
            				</div>
            				<div class="form-group">
            					<label for="DateOfBirth">Date of Birth</label>
            					<input type="date" class="form-control" id="DateOfBirth"  name="date_of_birth" placeholder="Date of Birth" required>
            				</div>
            				<div class="form-group">
            					<label for="Gender">Gender</label>
            					<input type="text" class="form-control" id="Gender"  name="gender" placeholder="Gender" required>
            				</div>
            				<div class="form-group">
            					<label for="Address">Address</label>
            					<input type="text" class="form-control" id="Address" name="address" placeholder="Address" required>
            				</div>
            				<div class="form-group">
            					<label for="Postcode">Postcode</label>
            					<input type="text" class="form-control" id="Postcode" name="postcode" placeholder="Postcode" required>
            				</div>
            				<div class="form-group">
            					<label for="MobileNo">Mobile Number</label>
            					<input type="text" class="form-control" id="MobileNo" name="mobile_number" placeholder="Mobile Number" required>
            				</div>
            				<div class="form-group">
            					<label for="EmailAddress">Email Address</label>
            					<input type="email" class="form-control" id="EmailAddress" name="email" placeholder="Email Address">
            				</div>
            				<div class="form-group">
            					<label for="NOKFirstName">Next of Kin First Name</label>
            					<input type="text" class="form-control" id="NOKFirstName" name="nok_first_name" placeholder="Next of Kin First Name" required>
            				</div>
            				<div class="form-group">
            					<label for="NOKLastName">Next of Kin Last Name</label>
            					<input type="text" class="form-control" id="NOKLastName" name="nok_last_name" placeholder="Next of Kin Last Name" required>
            				</div>
            				<div class="form-group">
            					<label for="NOKMobile">Next of Kin Mobile Number</label>
            					<input type="text" class="form-control" id="NOKMobile" name="nok_mobile" placeholder="Next of Kin Mobile Number" required>
            				</div>
            				<div class="form-group">
            					<label for="NOKAddress">Next of Kin Address</label>
            					<input type="text" class="form-control" id="NOKAddress" name="nok_address" placeholder="Next of Kin Address">
            				</div>
            				<div class="form-group">
            					<label for="NOKPostcode">Next of Kin Postcode</label>
            					<input type="text" class="form-control" id="NOKPostcode" name="nok_postcode" placeholder="Next of Kin Postcode">
            				</div>
            				<button type="submit" name="submit" class="btn btn-success mr-2">Submit</button>
                        	<button class="btn btn-light">Cancel</button>
            			</form>
            		</div>
            	</div>
            </div>
          </div>
          <div class="row">
          </div>
          <div class="row">
          </div>
          <div class="row">
          </div>
          <div class="row">
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright � 2018 404 Coursework Not Found. All rights reserved. Hand-crafted in
              <i class="mdi mdi-wordpress text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
<?php
?>