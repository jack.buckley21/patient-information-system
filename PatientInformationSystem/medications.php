<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Patient Information System</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="vendors/iconfonts/font-awesome/css/font-awesome.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="images/logo_thumbnail.png" />

<?php
include_once 'config.php';
session_start();
?>
<script>

function myFunction() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue, tbody;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("table table-striped");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
</head>

<body>
<?php

$sql = "select patient_personal_details.first_name, patient_personal_details.last_name, patient_medication.* FROM
patient_personal_details INNER JOIN patient_medication ON patient_personal_details.patient_id=patient_medication.patient_id";
$result = mysqli_query($mysqli, $sql);

$staff_surname = "";
$sql2 = "SELECT staff_last_name from staff_details where staff_id = " . $_SESSION['staffid'];
$resultSurname = mysqli_query($mysqli, $sql2);
$rows = mysqli_fetch_array($resultSurname);

foreach ($rows as $found) {
    $staff_surname = $found;
}

$staff_gender = "";
$staff_title = "";
$sql3 = "SELECT staff_gender from staff_details where staff_id = " . $_SESSION['staffid'];
$resultGender = mysqli_query($mysqli, $sql3);
$rows = mysqli_fetch_array($resultGender);

foreach ($rows as $found) {
    $staff_gender = $found;
}

if ($_SESSION['role'] == "Doctor") {
    $staff_title = "Dr. ";
} else if ($_SESSION['role'] == "Consultant") {
    $staff_title = "Dr.";
} else if ($_SESSION['role'] == "Lab Technician") {
    $staff_title = "Dr. ";
} else if ($_SESSION['role'] == "Nurse" && $staff_gender == "M") {
    $staff_title = "Mr. ";
} else if ($_SESSION['role'] == "Nurse" && $staff_gender == "F") {
    $staff_title = "Miss. ";
} else if ($_SESSION['role'] == "Receptionist" && $staff_gender == "M") {
    $staff_title = "Mr. ";
} else if ($_SESSION['role'] == "Receptionist" && $staff_gender == "F") {
    $staff_title = "Miss. ";
} else {
    echo "Error.";
}
?>
	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<nav
			class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
			<div
				class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
				<a class="navbar-brand brand-logo" href="dash.php"> <img
					src="images/logo.png" alt="logo" />
				</a> <a class="navbar-brand brand-logo-mini" href="dash.php"> <img
					src="images/logo_thumbnail.png" alt="logo" />
				</a>
			</div>
			<div class="navbar-menu-wrapper d-flex align-items-center">
				<ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
					<li class="nav-item"><a href="details.php" class="nav-link"> <i
							class="mdi mdi-account-details"></i>Patient Details
					</a></li>
					<li class="nav-item active"><a href="reports.php" class="nav-link">
							<i class="mdi mdi-elevation-rise"></i>Daily Report
					</a></li>
					<li class="nav-item active"><a href="immunisations.php"
						class="nav-link"> <i class="mdi mdi-needle"></i>Immunisations
					</a></li>
					<li class="nav-item"><a href="medications.php" class="nav-link"> <i
							class="mdi mdi-pill"></i>Medications
					</a></li>
					<li class="nav-item active"><a href="allergies.php"
						class="nav-link"> <i class="mdi mdi-flower"></i>Allergies
					</a></li>
					<li class="nav-item active"><a href="results.php" class="nav-link">
							<i class="mdi mdi-heart-pulse"></i>Test Results
					</a></li>
				</ul>
				<ul class="navbar-nav navbar-nav-right">
					<li class="nav-item dropdown d-none d-xl-inline-block"><a
						class="nav-link dropdown-toggle" id="UserDropdown" href="#"
						data-toggle="dropdown" aria-expanded="false"> <?php

            echo '<span class="profile-text">Hello, ' . $staff_title . $staff_surname . '</span>'?> <img
							class="img-xs rounded-circle" src="images/faces/face17.jpg"
							alt="Profile image">
					</a>
						<div class="dropdown-menu dropdown-menu-right navbar-dropdown"
							aria-labelledby="UserDropdown">
							<a class="dropdown-item p-0">
								<div class="d-flex border-bottom">
									<div
										class="py-3 px-4 d-flex align-items-center justify-content-center">
									</div>
								</div>
							</a> <a class="dropdown-item"> Change Password </a> <a
								class="dropdown-item"> Sign Out </a>
						</div></li>
				</ul>
				<button
					class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
					type="button" data-toggle="offcanvas">
					<span class="mdi mdi-menu"></span>
				</button>
			</div>
		</nav>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<!-- partial:partials/_sidebar.html -->
			<nav class="sidebar sidebar-offcanvas" id="sidebar">
				<ul class="nav">
					<li class="nav-item nav-profile">
						<div class="nav-link">
							<div class="user-wrapper">
								<div class="profile-image">
									<img src="images/faces/face17.jpg" alt="profile image">
								</div>
								<div class="text-wrapper">
                  <?php

                echo '<p class="profile-name">' . $staff_title . $staff_surname . '</p>'?>
                  <div>
										<?php

        echo '<small class="designation text-muted">' . $_SESSION['role'] . '</small>'?> <span
											class="status-indicator online"></span>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="nav-item"><a class="nav-link" href="dash.php"> <i
							class="menu-icon mdi mdi-television"></i> <span
							class="menu-title">Dashboard</span>
					</a></li>
          <?php
        if ($_SESSION["role"] == "Receptionist") {
            echo '<li class="nav-item">
            <a class="nav-link" href="newpatient.php">
              <i class="menu-icon mdi mdi-account-plus"></i>
              <span class="menu-title">Add New Patient</span>
            </a>
          </li>';
        }
        ?>
					<li class="nav-item"><a class="nav-link" href="details.php"> <i
							class="menu-icon mdi mdi-account-box"></i> <span
							class="menu-title">Patient Search</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="allmedications.php"> <i
							class="menu-icon mdi mdi-pill"></i> <span
							class="menu-title">View All Medications</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="allimmunisations.php"> <i
							class="menu-icon mdi mdi-needle"></i> <span
							class="menu-title">View all Immunisations</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="#"> <i
							class="menu-icon mdi mdi-alien"></i> <span class="menu-title">Extra
								Features Soon!</span>
					</a></li>
					<li class="nav-item">
						<div class="nav-link">
							<div class="user-wrapper">
								<div class="text-wrapper">
									<button class="btn btn-danger widthbtn"
										onclick="window.location.href='index.php'">
										Log Out <i class="mdi mdi-run"></i>
									</button>
								</div>
							</div>

					</li>
				</ul>
			</nav>
			<!-- partial -->

			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div
							class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
							<div class="card card-statistics">
								<div class="card-body">
									<div class="clearfix">
										<div class="float-right">
											<i class="mdi mdi-pill text-info icon-lg"></i>
										</div>
										<div class="float-left">
											<h1 class="display-4">Patient Medications</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Patients</h4>
									<p class="card-description"></p>
									<div class="table-responsive">
										<input type="text" id="myInput" onkeyup="myFunction()"
											placeholder="Enter Surname to Search for Patient..">
										<table class="table table-striped" id="table table-striped">
											<thead>
												<tr>
													<th>Patient ID</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>Medication ID</th>
													<th>Medication Dose (mg)</th>
													<th>Medication Start Date</th>
													<th>Medication End Date</th>
													<th>Medication Instructions</th>
												</tr>


											</thead>

											<tbody>
                     <?php
                    if (mysqli_num_rows($result) > 0) {
                        // output data of each ro
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<tr>';
                            echo "<td>" . $row["patient_id"] . "</td>";
                            echo "<td>" . $row["first_name"] . "</td>";
                            echo "<td>" . $row["last_name"] . "</td>";
                            echo "<td>" . $row["medication_id"] . "</td>";
                            echo "<td>" . $row["medication_dose"] . "</td>";
                            echo "<td>" . $row["medication_start_date"] . "</td>";
                            echo "<td>" . $row["medication_end_date"] . "</td>";
                            echo "<td>" . $row["medication_instructions"] . "</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "0 results";
                    }
                    
                    mysqli_close($mysqli);
                    ?>


                      </tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

				<!-- content-wrapper ends -->
				<!-- partial:partials/_footer.html -->
				<footer class="footer">
					<div class="container-fluid clearfix">
						<span
							class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright
							� 2018 404 Coursework Not Found. All rights reserved.
							Hand-crafted in <i class="mdi mdi-wordpress text-danger"></i>
						</span>
					</div>
				</footer>
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<!-- plugins:js -->
	<script src="vendors/js/vendor.bundle.base.js"></script>
	<script src="vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<script src="js/off-canvas.js"></script>
	<script src="js/misc.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script src="js/dashboard.js"></script>
	<!-- End custom js for this page-->

</body>

</html>
<?php
?>

<?php
?>